CREATE USER bookuser IDENTIFIED BY '123456';

GRANT ALL ON *.* TO bookuser;

CREATE DATABASE DB_BOOK;

ALTER USER 'bookuser'@'%' 
	IDENTIFIED WITH mysql_native_password BY '123456';

FLUSH PRIVILEGES;

USE DB_BOOK;

CREATE TABLE ServiceType(
       id INT AUTO_INCREMENT PRIMARY KEY,
       icon VARCHAR(100) NOT NULL,
       title VARCHAR(15) NOT NULL
);
-- drop table Technicain;
CREATE TABLE Technicain(
    id INT AUTO_INCREMENT PRIMARY KEY,
    company VARCHAR(90) NOT NULL,
    logo VARCHAR(100) NOT NULL,
    avatar VARCHAR(100) NOT NULL,
    nickname VARCHAR(30) NOT NULL,
    price INT NOT NULL,
    message VARCHAR(450) NOT NULL,
    detail VARCHAR(450) NOT NULL,
    distance INT NOT NULL
);

-- drop table Service;
CREATE TABLE Service(
    id INT AUTO_INCREMENT PRIMARY KEY,
    SUBJECT VARCHAR(60) NOT NULL,
    coverpath VARCHAR(100) NOT NULL,
    price INT NOT NULL,
    message VARCHAR(450) NOT NULL,
    serviceTypeId INT,
    technicainId INT,
    summry VARCHAR(300) NOT NULL,
    detail VARCHAR(450) NOT NULL,
    picture VARCHAR(100) NOT NULL,
    FOREIGN KEY fk1 (serviceTypeId) REFERENCES ServiceType(id),
    FOREIGN KEY fk2 (technicainId) REFERENCES Technicain(id) 
);

