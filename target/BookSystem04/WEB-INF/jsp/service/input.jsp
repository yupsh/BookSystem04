<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务管理</title>
        <base href="${pageContext.request.contextPath}/">
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/font.css">
        <link rel="stylesheet" href="css/xadmin.css">

        <script src="js/jquery.min.js"></script>
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
    </head>
    <body>
        <div class="main">
            <div class="content">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                    <legend>${act=="addSave"?"新建":"修改"}服务信息</legend>
                </fieldset>
                <h2>${msg}</h2>
                <form action="servlet/ServiceSaveAction?act=${act}" method="post"
                      enctype="multipart/form-data" class="layui-form">
                    <table class="layui-table">
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>服务名称</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                                <input name="id" type="hidden" value="${service.id}">
                                <input class="layui-input" name="subject" type="text" value="${service.subject}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务coverpath</td>
                            <td class="aleft">
                                <input name="coverpath" type="file" value="${service.coverpath}">
                                <img class="img" src="${initParam.uploadDir}/${service.coverpath}"
                                     alt="${service.subject}"></td>
                            </td>
                        </tr>
                        <tr>
                            <td>服务价格</td>
                            <td class="aleft">
                                <input class="layui-input" name="price" type="number" value="${service.price}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务口号</td>
                            <td class="aleft">
                                <textarea class="layui-textarea" name="message">${service.message}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务类型</td>
                            <td class="layui-card-body layui-row layui-col-space10">
                                <c:forEach var="serviceType" items="${serviceTypes}">
                                    <label layui-col-md12-my>
                                        <input ${service.serviceType.id==serviceType.id?'checked':''} 
                                                name="serviceTypeId" type="radio" value="${serviceType.id}"
                                        title="${serviceType.title}">
                                    </label>
                                </c:forEach>
                            </td>
                        </tr>
                        <tr>
                            <td>服务教练</td>
                            <td class="aleft">
                                <select name="coachId">
                                    <option value="0">请选择教练</option>
                                    <c:forEach var="coach" items="${coaches}">
                                        <option  ${service.coach.id==coach.id?"selected":""}
                                                value="${coach.id}">${coach.nickname}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>服务简介</td>
                            <td class="aleft">
                                <textarea class="layui-textarea" name="recommend">${service.recommend}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务详情</td>
                            <td class="aleft">
                                <textarea class="layui-textarea" name="detail">${service.detail}</textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>服务展示图</td>
                            <td class="aleft">
                                <input type="file" name="photo" value="${service.photo}">
                                <img class="img" src="${initParam.uploadDir}/${service.photo}"
                                     alt="${service.subject}"></td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="page">
                                <input class="layui-btn layui-btn-primary" type="submit" style="border:0;" value="保存"/>
                                <input class="layui-btn layui-btn-primary" type="reset" style="border:0;" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
