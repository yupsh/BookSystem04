<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>${initParam.appCation}-教练信息管理</title>
        <base href="${pageContext.request.contextPath}/">
<%--        <link rel="stylesheet" href="style/base.css" />--%>
<%--        <link rel="stylesheet" href="style/style.css" />--%>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/font.css">
        <link rel="stylesheet" href="css/xadmin.css">

        <script src="js/jquery.min.js"></script>
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
    </head>
    <body>
<%--        <%@ include file="../head.jsp" %>--%>
        <div class="main">
<%--            <%@ include file="../nav.jsp" %>--%>
            <div class="content">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                    <legend>教练列表</legend>
                </fieldset>
                <h2>${msg}</h2>
                    <table class="layui-table">
                        <tr>
                            <th>编号</th>
                            <th>公司</th>
                            <th>头像</th>
                            <th>照片</th>
                            <th>昵称</th>
                            <th>价格</th>
                            <th>介绍</th>
                            <th>距离</th>
                            <th>删除</th>
                            <th>修改</th>
                        </tr>
                        <c:forEach var="coach" items="${coaches}">
                            <tr>
                                <td>${coach.id}</td>
                                <td>${coach.company}</td>
<%--                                <td>${coach.photo}</td>--%>
                                <td>
                                    <img class="img" src="${initParam.uploadDir}/${coach.photo}" alt="${coach.nickname}"></td>
                                </td>
<%--                                <td>${coach.avatar}</td>--%>
                                <td>
                                    <img class="img" src="${initParam.uploadDir}/${coach.avatar}" alt="${coach.nickname}"></td>
                                </td>
                                <td>${coach.nickname}</td>
                                <td>${coach.price}</td>
                                <td>${coach.message}</td>
                                <td>-1</td>
                                <td><a onclick="return confirm('确认删除吗?')"
                                       href="servlet/CoachAction?act=delete&id=${coach.id}">
                                    <button class="layui-btn layui-btn-danger">
                                        <i class="layui-icon">&#xe640;</i>
                                    </button>
                                </a></td>
                                <td><a href="servlet/CoachAction?act=update&id=${coach.id}">
                                    <button class="layui-btn">
                                        <i class="layui-icon">&#xe642;</i>
                                    </button>
                                </a></td>
                            </tr>
                        </c:forEach>
                    </table>
            </div>
        </div>
    </body>
</html>
