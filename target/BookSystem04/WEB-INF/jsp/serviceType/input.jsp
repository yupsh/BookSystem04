<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-服务类型管理</title>
        <base href="${pageContext.request.contextPath}/">
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/font.css">
        <link rel="stylesheet" href="css/xadmin.css">

        <script src="js/jquery.min.js"></script>
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
    </head>
    <body>
        <div class="main">
            <div class="content">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                    <legend>${act=="addSave"?"新建":"修改"}服务类型</legend>
                </fieldset>
                <h2>${msg}</h2>
                <form action="servlet/ServiceTypeSaveAction?act=${act}" method="post" enctype="multipart/form-data">
                    <table class="layui-table">
                        <tr>
                            <td>服务名称</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                                <input name="id" type="hidden" value="${serviceType.id}">
                                <input class="layui-input" name="title" type="text" value="${serviceType.title}">
                            </td>
                        </tr>
                        <tr>
                            <td>服务Icon</td>
                            <td class="aleft">
                                <input name="icon" type="file" value="${serviceType.icon}">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="page">
                                <input class="layui-btn layui-btn-primary" type="submit" style="border:0;" value="保存"/>
                                <input class="layui-btn layui-btn-primary" type="reset" style="border:0;" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
