package com.yups.dao;

import com.yups.model.Carousels;
import com.yups.model.Service;
import org.junit.Test;

import static org.junit.Assert.*;

public class CarouselsDaoTest {
	private CarouselsDao carouselsDao = new CarouselsDao();
	@Test
	public void add() {
		assertTrue(carouselsDao.add(new Carousels(1,"ceshi",new Service(1))));
	}

	@Test
	public void delete() {
		assertTrue(carouselsDao.delete(new Carousels(1)));
	}

	@Test
	public void update() {
		assertTrue(carouselsDao.update(new Carousels(1,"ceshi2",new Service(1))));
	}

	@Test
	public void get() {
		System.out.println(carouselsDao.get(new Carousels(1)));
	}

	@Test
	public void getAll() {
		System.out.println(carouselsDao.getAll().size());
	}
}