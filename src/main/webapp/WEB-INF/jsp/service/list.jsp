<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>${initParam.appCation}-服务类型管理</title>
  <base href="${pageContext.request.contextPath}/">
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
  <meta http-equiv="Cache-Control" content="no-siteapp"/>

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
  <link rel="stylesheet" href="css/font.css">
  <link rel="stylesheet" href="css/xadmin.css">

  <script src="js/jquery.min.js"></script>
  <script src="lib/layui/layui.js" charset="utf-8"></script>
  <script type="text/javascript" src="js/xadmin.js"></script>
</head>
<body>
<div class="main">
  <div class="content">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
      <legend>服务类型列表</legend>
    </fieldset>
    <h2>${msg}</h2>
    <table class="layui-table">
      <tr>
        <th>服务编号</th>
        <th>服务名称</th>
        <th>服务coverpath</th>
        <th>服务价格</th>
        <th>服务口号</th>
        <th>服务类型</th>
        <th>服务教练</th>
        <th>服务简介</th>
        <th>服务详情</th>
        <th>服务展示图</th>
        <th>删除</th>
        <th>修改</th>
      </tr>
      <c:forEach var="service" items="${services}">
        <tr>
          <td>${service.id}</td>
          <td>${service.subject}</td>
          <td>
            <img class="img" src="${initParam.uploadDir}/${service.coverpath}"
                 alt="${service.subject}"></td>
          <td>${service.price}</td>
          <td>${service.message}</td>
          <td>${service.serviceType.title}</td>
          <td>${service.coach.nickname}</td>
          <td>${service.recommend}</td>
          <td>${service.detail}</td>
          <td>
            <img class="img" src="${initParam.uploadDir}/${service.photo}" alt="${service.subject}">
          </td>
          </td>
          <td>
            <a onclick="return confirm('确认删除吗?')"
               href="servlet/ServiceAction?act=delete&id=${service.id}">
              <button class="layui-btn layui-btn-danger">
                <i class="layui-icon">&#xe640;</i>
              </button>
            </a></td>
          <td><a href="servlet/ServiceAction?act=update&id=${service.id}">
            <button class="layui-btn">
              <i class="layui-icon">&#xe642;</i>
            </button>
          </a></td>
        </tr>
      </c:forEach>
      <tr>
        <td colspan="12" class="page">
          <c:if test="${page!=1}">
            <a href="servlet/ServiceAction?act=list&page=1">首页</a>
            <a href="servlet/ServiceAction?act=list&page=${page-1}">上一页</a>
          </c:if>
          <c:if test="${page==1}">
            <label>首页</label>
            <label>上一页</label>
          </c:if>
          <label>第${page}页</label>
          <c:if test="${page!=pageCount}">
            <a href="servlet/ServiceAction?act=list&page=${page+1}">下一页</a>
            <a href="servlet/ServiceAction?act=list&page=${pageCount}">末页</a>
          </c:if>
          <c:if test="${page==pageCount}">
            <label>下一页</label>
            <label>末页</label>
          </c:if>
          <form action="servlet/ServiceAction" method="post" style="display: inline-block">
            <input name="page" type="number" style="width: 50px;">
              <label><input type="submit" value="跳转" style="border: 0"></label>
          </form>
        </td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
