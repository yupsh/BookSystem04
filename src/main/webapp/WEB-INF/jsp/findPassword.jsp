<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>${initParam.appCation}</title>
  <base href="${pageContext.request.contextPath}/">
  <link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
<div id="wrapper" class="login-page">
  <div id="login_form" class="form">
    <form action="FindPasswordAction" class="login-form" method="post">
      <h2>找回密码</h2>
      <p class="message" style="color: #CF1900">${msg}</p>
      <input name="mob" type="text" placeholder="输入手机号"/>
      <input name="email" type="text" placeholder="电子邮箱"/>
      <button id="login">提　交</button>
      <p class="message" style="text-align: right"><a href="login.jsp">返回登录</a></p>
    </form>
  </div>
</div>
</body>
</html>
