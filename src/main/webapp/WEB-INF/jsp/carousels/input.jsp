<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-轮播图管理</title>
        <base href="${pageContext.request.contextPath}/">
<%--        <link rel="stylesheet" href="style/base.css" />--%>
<%--        <link rel="stylesheet" href="style/style.css" />--%>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/font.css">
        <link rel="stylesheet" href="css/xadmin.css">

        <script src="js/jquery.min.js"></script>
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
    </head>
    <body>
<%--        <%@ include file="../head.jsp" %>--%>
        <div class="main">
<%--            <%@ include file="../nav.jsp" %>--%>
            <div class="content">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                    <legend>${act=="addSave"?"新建":"修改"}轮播图</legend>
                </fieldset>
                <h2>${msg}</h2>
                <form action="servlet/CarouselsSaveAction?act=${act}" method="post"
                      enctype="multipart/form-data" class="layui-form">
                    <table class="layui-table">
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>轮播图</td>
                            <td class="">
                                <input name="id" type="hidden" value="${carousels.id}">
                                <input name="icon" type="file" value="${carousels.icon}">
                                <img class="img" src="${initParam.uploadDir}/${carousels.icon}"
                                     alt="${carousels.id}">
                            </td>
                        </tr>
                        <tr class="layui-card layui-form">
                            <td>服务编号</td>
                            <td class="layui-card-body layui-row layui-col-space10">
                                <c:forEach var="service" items="${services}">
                                    <label class="layui-col-md12-my">
                                        <input ${carousels.service.id==service.id?'checked':''}
                                                name="serviceId" type="radio" value="${service.id}" title="${service.id}">
                                    </label>
                                </c:forEach>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="page">
                               <input class="layui-btn layui-btn-primary" type="submit" style="border:0;" value="保存"/>
                                <input class="layui-btn layui-btn-primary" type="reset" style="border:0;" value="取消" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
