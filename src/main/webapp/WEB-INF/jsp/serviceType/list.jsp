<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>${initParam.appCation}-服务类型管理</title>
  <base href="${pageContext.request.contextPath}/">
  <meta name="renderer" content="webkit|ie-comp|ie-stand">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport"
        content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
  <meta http-equiv="Cache-Control" content="no-siteapp"/>

  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
  <link rel="stylesheet" href="css/font.css">
  <link rel="stylesheet" href="css/xadmin.css">

  <script src="js/jquery.min.js"></script>
  <script src="lib/layui/layui.js" charset="utf-8"></script>
  <script type="text/javascript" src="js/xadmin.js"></script>
</head>
<body>
<div class="main">
  <div class="layui-form">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
      <legend>服务类型列表</legend>
    </fieldset>
    <h2>${msg}</h2>
    <table class="layui-table">
      <tr>
        <th>类型编号</th>
        <th>类型标题</th>
        <th>类型Icon</th>
        <th>删除</th>
        <th>修改</th>
      </tr>
      <c:forEach var="serviceType" items="${serviceTypes}">
        <tr>
          <td>${serviceType.id}</td>
          <td>${serviceType.title}</td>
            <%--                                <td>${serviceType.icon}</td>--%>
          <td>
            <img class="img" src="${initParam.uploadDir}/${serviceType.icon}"
                 alt="${serviceType.title}"></td>
          </td>
          <td><a onclick="return confirm('确认删除吗?')"
                 href="servlet/ServiceTypeAction?act=delete&id=${serviceType.id}">
            <button class="layui-btn layui-btn-danger">
              <i class="layui-icon">&#xe640;</i>
            </button>
          </a></td>
          <td><a href="servlet/ServiceTypeAction?act=update&id=${serviceType.id}">
            <button class="layui-btn">
              <i class="layui-icon">&#xe642;</i>
            </button>
          </a></td>
        </tr>
      </c:forEach>
    </table>
  </div>
</div>
</body>
</html>
