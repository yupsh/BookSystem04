<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>${initParam.appCation}-教练信息管理</title>
        <base href="${pageContext.request.contextPath}/">
<%--        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/base.css" />--%>
<%--        <link rel="stylesheet" href="${pageContext.request.contextPath}/style/style.css" />--%>
        <meta name="renderer" content="webkit|ie-comp|ie-stand">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/font.css">
        <link rel="stylesheet" href="css/xadmin.css">

        <script src="js/jquery.min.js"></script>
        <script src="lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="js/xadmin.js"></script>
        <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=您申请的key值"></script>
        <style>
            #map{
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0px;
                left: 0px;
                display: none;
            }
            #map [type="button"]{
                position: absolute;
                padding: 5px;
                z-index: 99;
                top: 5px;
                right: 5px;
            }
            #container {
                width: 100%;
                height: calc(100% - 20px);
            }
        </style>
    </head>
    <body>
        <div class="main">
            <div class="content">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
                    <legend>${act=="addSave"?"新建":"修改"}教练信息</legend>
                </fieldset>
                <h2>${msg}</h2>
                <form action="servlet/CoachSaveAction?act=${act}" method="post"
                      enctype="multipart/form-data" class="layui-form">
                <table class="layui-table">
                            <td>公司名称</td>
                            <td class="aleft">
                                <%--隐藏表单域--%>
                              <input name="id" type="hidden" readonly="true" value="${coach.id}">
                                <input class="layui-input" name="company" type="text" value="${coach.company}">
                            </td>
                        </tr>
                        <tr>
                            <td>头像</td>
                            <td class="aleft">
                                <input name="photo" type="file" value="${coach.photo}">
                            </td>
                        </tr>
                        <tr>
                            <td>照片</td>
                            <td class="aleft">
                                <input name="avatar" type="file" value="${coach.avatar}">
                            </td>
                        </tr>
                        <tr>
                            <td>昵称</td>
                            <td class="aleft">
                                <input class="layui-input" name="nickname" type="text" value="${coach.nickname}">
                            </td>
                        </tr>
                        <tr>
                            <td>价格</td>
                            <td class="aleft">
                                <input class="layui-input" name="price" type="text" value="${coach.price}">
                            </td>
                        </tr>
                        <tr>
                            <td>介绍</td>
                            <td class="aleft">
                                <input class="layui-input" name="message" type="text" value="${coach.message}">
                            </td>
                        </tr>
                        <tr>
                            <td>所在位置</td>
                            <td class="aleft">
                                纬度：<input class="layui-select" style="width: 80px;" id="lat" name="lat" type="text" readonly>
                                经度：<input class="layui-select" style="width: 80px;" id="lon" name="lon" type="text" readonly>
                                <input style="padding: 5px" name="nickname" type="button" value="打开地图..." onclick="openMap()">
                            </td>
                        <tr>
                    <td colspan="2" class="page">
                        <input class="layui-btn layui-btn-primary" type="submit" style="border:0;" value="保存"/>
                        <input class="layui-btn layui-btn-primary" type="reset" style="border:0;" value="取消" />
                    </td>
                </tr>
                    </table>
                </form>
            </div>
        </div>
        <div id="map">
            <input type="button" value="X" onclick="closeMap()">
            <div id="container"></div>
        </div>
    </body>
    <script type="text/javascript">
        var map = new AMap.Map('container', {
            zoom: 13
        });
        map.on('click', function(e) {
            console.log(e.lnglat);
            document.getElementById("lat").value=e.lnglat.lat;
            document.getElementById("lon").value=e.lnglat.lng;
            var marker = new AMap.Marker({
                icon: 'https://a.amap.com/jsapi_demos/static/demo-center/icons/poi-marker-default.png',
                position: [e.lnglat.lng, e.lnglat.lat],
                offset: new AMap.Pixel(-25, -60)
            });
            marker.setMap(map);
            setTimeout(function(){
                marker.setMap(null);
            },2000);
        })
        function openMap(){
            document.getElementById("map").style.display="block";
        }
        function closeMap(){
            document.getElementById("map").style.display="none";
        }
    </script>
</html>
