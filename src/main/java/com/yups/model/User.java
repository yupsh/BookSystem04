package com.yups.model;

/**
 * 用户类
 *
 * @author 于鹏生
 */
public class User {
	private int id;
	private String nickName;

	public User() {
	}

	public User(int id) {
		super();
		this.id = id;
	}

	public User(String nickName) {
		super();
		this.nickName = nickName;
	}

	public User(int id, String nickName) {
		super();
		this.id = id;
		this.nickName = nickName;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nickName=" + nickName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
}
