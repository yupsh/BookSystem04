package com.yups.model;
import java.sql.Timestamp;

/**
 * 预约
 *
 * @author 于鹏生
 */
public class Book {
	private int id;
	private Service service;
	private User user;
	private Timestamp bookTime;
	private long price;
	private String message;

	public Book() {
	}
	
	public Book(int id, Service service, User user, Timestamp bookTime, long price,
				String message) {
		this.id = id;
		this.service = service;
		this.user = user;
		this.bookTime = bookTime;
		this.price = price;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", service=" + service 
				+ ", user=" + user + ", bookTime=" + bookTime + ", price="
				+ price + ", message=" + message + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getBookTime() {
		return bookTime;
	}

	public void setBookTime(Timestamp bookTime) {
		this.bookTime = bookTime;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
