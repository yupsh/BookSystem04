package com.yups.model;

/**
 * 轮播图
 *
 * @author 于鹏生
 */
public class Carousels {
	private int id;
	private String icon;
	private Service service;

	public Carousels() {
	}

	public Carousels(int id) {
		this.id = id;
	}

	public Carousels(int id, String icon, Service service) {
		this.id = id;
		this.icon = icon;
		this.service = service;
	}

	@Override
	public String toString() {
		return "Carousels [id=" + id + ", icon=" + icon
				+ ", service=" + service + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carousels other = (Carousels) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}
