package com.yups.model;

/**
 * 服务类
 *
 * @author 于鹏生
 */
public class Service {
	private int id;
	private String subject;
	private String photo;
	private String coverpath;
	private double price;
	private String message;
	private String recommend;
	private String detail;
	private Coach coach;
	private ServiceType serviceType;

	public Service() {
		super();
	}

	public Service(int id) {
		super();
		this.id = id;
	}

	public Service(int id, String subject, String photo, String coverpath,
				   double price, String message, String recommend, String detail,
				   Coach coach, ServiceType serviceType) {
		super();
		this.id = id;
		this.subject = subject;
		this.photo = photo;
		this.coverpath = coverpath;
		this.price = price;
		this.message = message;
		this.recommend = recommend;
		this.detail = detail;
		this.coach = coach;
		this.serviceType = serviceType;
	}

	@Override
	public String toString() {
		return "Service [id=" + id + ", subject="
				+ subject + ", photo=" + photo + ", coverpath="
				+ coverpath + ", price=" + price + ", message="
				+ message + ", recommend=" + recommend + ", detail="
				+ detail + ", coach=" + coach + ", serviceType="
				+ serviceType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getCoverpath() {
		return coverpath;
	}

	public void setCoverpath(String coverpath) {
		this.coverpath = coverpath;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRecommend() {
		return recommend;
	}

	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}
}