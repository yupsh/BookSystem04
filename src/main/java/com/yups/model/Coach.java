package com.yups.model;
import java.util.Objects;

/**
 * 教练类
 *
 * @author 于鹏生
 * @date 2021/11/4 17:30
 */
public class Coach {
	private int id;
	private String company;
	private String photo;
	private String avatar;
	private String nickname;
	private double price;
	private String message;
	private double lat;
	private double lon;
	private long distance;

	public Coach() {
	}

	public Coach(int id) {
		this.id = id;
	}

	public Coach(int id, String company, String photo, String avatar,String nickname,
				 double price, String message, double lat, double lon) {
		this.id = id;
		this.company = company;
		this.photo = photo;
		this.avatar = avatar;
		this.nickname = nickname;
		this.price = price;
		this.message = message;
		this.lat = lat;
		this.lon = lon;
	}

	public Coach(int id, String company, String photo, String avatar, String nickname,
				 double price, String message, double lat, double lon, long distance) {
		this.id = id;
		this.company = company;
		this.photo = photo;
		this.avatar = avatar;
		this.nickname = nickname;
		this.price = price;
		this.message = message;
		this.lat = lat;
		this.lon = lon;
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "Coach{" +
				"id=" + id +
				", company='" + company + '\'' +
				", photo='" + photo + '\'' +
				", avatar='" + avatar + '\'' +
				", nickname='" + nickname + '\'' +
				", price=" + price +
				", message='" + message + '\'' +
				", lat=" + lat +
				", lon=" + lon +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Coach coach = (Coach) o;
		return id == coach.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}
}
