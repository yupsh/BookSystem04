package com.yups.model;

import java.util.Objects;

/**
 * 用户
 * @author 于鹏生
 */
public class Emp {
    private int id;
    private String name;
    private String mob;
    private String password;
    private String email;

    public Emp() {
    }

    public Emp(String name, String mob, String password) {
        this.name = name;
        this.mob = mob;
        this.password = password;
    }

    public Emp(int id, String name, String mob, String password, String email) {
        this.id = id;
        this.name = name;
        this.mob = mob;
        this.password = password;
        this.email = email;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mob='" + mob + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Emp emp = (Emp) o;
        return id == emp.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
