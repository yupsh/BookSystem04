package com.yups.service;

import com.yups.dao.CoachDao;
import com.yups.dao.ServiceDao;
import com.yups.dao.ServiceTypeDao;
import com.yups.model.Coach;
import com.yups.model.Service;
import com.yups.model.ServiceType;

import java.util.List;

/**
 * 服务类 Service
 *
 * @author 于鹏生
 */
public class ServiceService {
	private ServiceDao serviceDao = new ServiceDao();
	private ServiceTypeDao serviceTypeDao = new ServiceTypeDao();
	private CoachDao coachDao = new CoachDao();


	public boolean add(Service service) {
		return serviceDao.add(service);
	}

	public boolean delete(int id) {
		return serviceDao.delete(id);
	}

	public Service getService(int id) {
		return serviceDao.get(id);
	}

	public boolean update(Service service) {
		return serviceDao.update(service);
	}

	public List<Service> getServices() {
		return serviceDao.getAll();
	}

	public List<Service> getServices(int size, long page) {
		return serviceDao.getAll(size, page);
	}

	public List<ServiceType> getServiceTypes() {
		return serviceTypeDao.getAll();
	}

	public List<Coach> getCoaches() {
		return coachDao.getAll();
	}

	public Long getPageCount(int size) {
		return serviceDao.getPageCount(size);
	}
}
