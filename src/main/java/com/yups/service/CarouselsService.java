package com.yups.service;
import com.yups.dao.CarouselsDao;
import com.yups.dao.ServiceDao;
import com.yups.model.Carousels;
import com.yups.model.Service;
import java.util.List;

/**
 * 轮播图service
 *
 * @author 于鹏生
 */
public class CarouselsService {
	private ServiceDao serviceDao = new ServiceDao();
	private CarouselsDao carouselsDao = new CarouselsDao();

	public boolean add(Service service) {
		return serviceDao.add(service);
	}

	public boolean delete(int id) {
		return serviceDao.delete(id);
	}

	public Service getService(int id) {
		return serviceDao.get(id);
	}

	public boolean update(Service service) {
		return serviceDao.update(service);
	}

	public List<Service> getServices() {
		return serviceDao.getAll();
	}

	public List<Service> getServices(int size, long page) {
		return serviceDao.getAll(size, page);
	}

	public List<Carousels> getCarouselses() {
		return carouselsDao.getAll();
	}

	public Long getPageCount(int size) {
		return serviceDao.getPageCount(size);
	}

	public boolean addCarousels(Carousels carousels) {
		return carouselsDao.add(carousels);
	}

	public boolean deleteCarousels(Carousels carousels) {
		return carouselsDao.delete(carousels);
	}

	public Carousels getCarousels(Carousels carousels) {
		return carouselsDao.get(carousels);
	}

	public boolean updateCarousels(Carousels carousels) {
		return carouselsDao.update(carousels);
	}
}
