package com.yups.wxweb;

import com.yups.dao.CoachDao;
import com.yups.model.Coach;
import net.sf.json.JSONArray;

import javax.servlet.annotation.WebServlet;
import java.util.List;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetCoachItems")
public class GetCoachItems extends JsonServlet {
	private CoachDao coachDao = new CoachDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		List<Coach> coachList = coachDao.getAll();
		for (Coach coach : coachList) {
			coach.setAvatar(IMG_PATH + coach.getAvatar());
			coach.setPhoto(IMG_PATH + coach.getPhoto());
		}
		return JSONArray.fromObject(coachList).toString();
	}
}
