package com.yups.wxweb;

import com.yups.dao.CoachDao;
import com.yups.model.Coach;
import net.sf.json.JSONObject;
import javax.servlet.annotation.WebServlet;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetCoach")
public class GetCoach extends JsonServlet {
	private CoachDao coachDao = new CoachDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		int id = 1;
		if (input.get("id") != null 
				&& input.get("id")[0].matches("\\d{1,9}")) {
			id = Integer.parseInt(input.get("id")[0]);
		}
		Coach coachById = coachDao.getCoachById(id);
		coachById.setAvatar(IMG_PATH + coachById.getAvatar());
		coachById.setPhoto(IMG_PATH + coachById.getPhoto());
		return JSONObject.fromObject(coachById).toString();
	}
}
