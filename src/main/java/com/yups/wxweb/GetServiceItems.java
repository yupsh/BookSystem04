package com.yups.wxweb;

import com.yups.dao.ServiceDao;
import com.yups.model.Service;
import net.sf.json.JSONArray;

import javax.servlet.annotation.WebServlet;
import java.util.*;


/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetServiceItems")
public class GetServiceItems extends JsonServlet {
	private ServiceDao serviceDao = new ServiceDao();

	@Override
	public String dealInput(Map<String, String[]> input) {
		int typeId = 1;
		if (input.get("typeId") != null &&
				input.get("typeId")[0].matches("\\d{1,9}")) {
			typeId = Integer.parseInt(input.get("typeId")[0]);
		}
		List<Service> services = serviceDao.getServiceByTypeID(typeId);
		for (Service service : services) {
			service.setPhoto(IMG_PATH + service.getPhoto());
			service.setCoverpath(IMG_PATH + service.getCoverpath());
		}
		return JSONArray.fromObject(services).toString();
	}
}
