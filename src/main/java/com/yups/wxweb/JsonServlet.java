package com.yups.wxweb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author 于鹏生
 */
public abstract class JsonServlet extends HttpServlet {
	protected final String IMG_PATH = "http://localhost:9090/BookSystem04/upload/";
	private static final long serialVersionUID = 1L;
	private static final String CHARSET = "UTF-8";
    public JsonServlet() {
        super();
    }
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding(CHARSET);
		response.setContentType("application/json;charset="+CHARSET);
		String input =request.getParameter("content");
		Map<String, String[]> parameterMap = request.getParameterMap();
		PrintWriter out = response.getWriter();
		out.write(dealInput(parameterMap));
		out.flush();
		out.close();
	}

	public abstract String dealInput(Map<String, String[]> input);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request,response);
	}
}
