package com.yups.wxweb;

import com.yups.dao.BookDao;
import com.yups.dao.ServiceDao;
import com.yups.model.Book;
import com.yups.model.Service;
import com.yups.model.User;
import org.apache.commons.lang.StringUtils;

import javax.servlet.annotation.WebServlet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/BookServiceAction")
public class BookServiceAction extends JsonServlet {
	private ServiceDao serviceDao = new ServiceDao();

	@Override
	public String dealInput(Map<String, String[]> input) {
		Book book = new Book();
		String serviceId = input.get("serviceId")[0];
		if (serviceId != null && serviceId.matches("\\d{1,9}")) {
			Service service = serviceDao.getServiceByID(Integer.parseInt(serviceId));
			book.setService(service);
			book.setPrice((long)service.getPrice());
		}
		book.setMessage(input.get("message")[0]);
		String nickName = input.get("nickName")[0];
		if (StringUtils.isNotBlank(nickName)) {
			User user = new User(nickName);
			book.setUser(user);
		}
		String date = input.get("date")[0];
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			Date parse = sdf.parse(date);
			System.out.println("格式化日期" + parse);
			if ((parse.getTime() - System.currentTimeMillis()) > 0) {
				System.out.println("这是个成功的预约");
			} else {
				System.out.println("预约失败");
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(date);
		if (date != null && 
				date.matches("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}")) {
			Timestamp bookTime = Timestamp.valueOf(date + ":00");
			book.setBookTime(bookTime);
		}
		// if (book.getBookTime() > )
		System.out.println("时间戳" + book.getBookTime());
		BookDao bookDao = new BookDao();
		if (bookDao.addBook(book)) {
			return "预约成功";
		}
		return "预约失败";
		// System.out.println(book);
		// return JSONObject.fromObject(book).toString();
	}
}
