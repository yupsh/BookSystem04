package com.yups.wxweb;

import com.yups.dao.BookDao;
import com.yups.utils.JsonDateValueProcessor;
import net.sf.json.JSONArray;

import javax.servlet.annotation.WebServlet;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetBookItems")
public class GetBookItems extends JsonServlet {
	private BookDao bookDao = new BookDao();

	@Override
	public String dealInput(Map<String, String[]> input) {
		JsonDateValueProcessor jsonDateValueProcessor =
				new JsonDateValueProcessor();
		return JSONArray.fromObject(bookDao.getBook()).toString();
	}
}
