package com.yups.wxweb;
import com.yups.dao.ServiceDao;
import com.yups.model.Coach;
import com.yups.model.Service;
import net.sf.json.JSONArray;
import javax.servlet.annotation.WebServlet;
import java.util.List;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetServiceByCoachId")
public class GetServiceByCoachId extends JsonServlet {
	private ServiceDao serviceDao = new ServiceDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		int id = 1;
		if (input.get("id") != null 
				&& input.get("id")[0].matches("\\d{1,9}")) {
			id = Integer.parseInt(input.get("id")[0]);
		}
		List<Service> serviceByCoachId = serviceDao.getServiceByCoachId(id);
		for (Service service : serviceByCoachId) {
			Coach coach = service.getCoach();
			coach.setAvatar(IMG_PATH + coach.getAvatar());
			coach.setPhoto(IMG_PATH + coach.getPhoto());
			service.setCoverpath(IMG_PATH + service.getCoverpath());
			service.setPhoto(IMG_PATH + service.getPhoto());
		}
		return JSONArray.fromObject(serviceByCoachId).toString();
	}
}
