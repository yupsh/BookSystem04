package com.yups.wxweb;
import com.yups.dao.ServiceTypeDao;
import com.yups.model.ServiceType;
import net.sf.json.JSONArray;
import javax.servlet.annotation.WebServlet;
import java.util.List;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetNavTopItems")
public class GetNavTopItems extends JsonServlet {
	private ServiceTypeDao serviceTypeDao = new ServiceTypeDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		List<ServiceType> serviceTypeList = serviceTypeDao.getAll();
		for (ServiceType serviceType : serviceTypeList) {
			serviceType.setIcon(IMG_PATH + serviceType.getIcon());
		}
		return JSONArray.fromObject(serviceTypeList).toString();
	}
}
