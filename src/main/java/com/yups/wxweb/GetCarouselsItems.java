package com.yups.wxweb;
import com.yups.dao.CarouselsDao;
import com.yups.model.Carousels;
import com.yups.utils.JsonDateValueProcessor;
import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import javax.servlet.annotation.WebServlet;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetCarouselsItems")
public class GetCarouselsItems extends JsonServlet  {
	private CarouselsDao carouselsDao = new CarouselsDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, 
        		new JsonDateValueProcessor());
		List<Carousels> carouselsList = carouselsDao.getAll();
		for (Carousels carousels : carouselsList) {
			carousels.setIcon(IMG_PATH + carousels.getIcon());
		}
		return JSONArray.fromObject(carouselsList,jsonConfig).toString();
	}
}
