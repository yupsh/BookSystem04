package com.yups.wxweb;

import com.yups.dao.ServiceDao;
import com.yups.model.Service;
import net.sf.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import java.util.Map;

/**
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/wx/GetService")
public class GetService extends JsonServlet {
	private ServiceDao serviceDao = new ServiceDao();
	@Override
	public String dealInput(Map<String, String[]> input) {
		int id = 1;
		if (input.get("id") != null && input.get("id")[0].matches("\\d{1,9}")) {
			id = Integer.parseInt(input.get("id")[0]);
		}
		Service service = serviceDao.getServiceByID(id);
		service.setPhoto(IMG_PATH + service.getPhoto());
		service.setCoverpath(IMG_PATH + service.getCoverpath());
		return JSONObject.fromObject(service).toString();
	}
}
