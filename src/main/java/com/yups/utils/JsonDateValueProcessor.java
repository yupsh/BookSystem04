package com.yups.utils;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * json 时间戳格式化
 *
 * @author 于鹏生
 */
public class JsonDateValueProcessor implements JsonValueProcessor {
    private String format = "yyyy-MM-dd HH:mm:dd";  
  
    public JsonDateValueProcessor() {  
    }  
  
    public JsonDateValueProcessor(String format) {  
         this.format = format;  
    }  
  
    @Override  
    public Object processArrayValue(Object arg0, JsonConfig arg1) {
        return process(arg0);  
    }  
  
    private Object process(Object arg0) {  
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(arg0);  
    }  
  
    @Override  
    public Object processObjectValue(String key, Object value,  
            JsonConfig jsonConfig) {  
        if (value instanceof Date) {
            String str = new SimpleDateFormat(format).format((Date) value);
            return str;  
        }  
        if (null != value) {  
            return value.toString();  
        }  
        return "";  
    }  
}