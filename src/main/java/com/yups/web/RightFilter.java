package com.yups.web;

import com.yups.model.Emp;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = "/servlet/*")
public class RightFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws ServletException, IOException {
        //判断是否登录
        Emp loginEmp = (Emp) ((HttpServletRequest) req).getSession().getAttribute("loginEmp");
        if(loginEmp!=null){
            chain.doFilter(req, resp);
        }else{
            req.setAttribute("msg","您还没有登录或者登录已经超时。");
            req.getRequestDispatcher("../login.jsp").forward(req,resp);
        }
    }
}
