package com.yups.web;

import com.yups.model.Carousels;
import com.yups.model.Service;
import com.yups.service.CarouselsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = "/servlet/CarouselsSaveAction")
@MultipartConfig
public class CarouselsSaveAction extends HttpServlet {
	private CarouselsService carouselsService = new CarouselsService();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String act = request.getParameter("act");
		if (act == null) {
			act = "list";
		}
		Carousels carousels = getCarouselsByRequest(request);
		switch (act) {
			case "addSave":
				String msg = "";
				if (!"".equals(msg)) {
					request.setAttribute("act", "addSave");
					request.setAttribute("carousels", carousels);
					forward(request, response, msg, "../WEB-INF/jsp/carousels/input.jsp");
					return;
				}
				if (carouselsService.addCarousels(carousels)) {
					forward(request, response, "保存成功", "CarouselsAction?act=list");
				} else {
					request.setAttribute("act", "addSave");
					request.setAttribute("carousels", carousels);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/carousels/input.jsp");
				}
				break;
			case "updateSave":
				msg = "";
				if (!"".equals(msg)) {
					request.setAttribute("act", "updateSave");
					request.setAttribute("carousels", carousels);
					forward(request, response, msg, "../WEB-INF/jsp/carousels/input.jsp");
					return;
				}
				if (carouselsService.updateCarousels(carousels)) {
					forward(request, response, "保存成功", "CarouselsAction?act=list");
				} else {
					request.setAttribute("act", "updateSave");
					request.setAttribute("carousels", carousels);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/carousels/input.jsp");
				}
				break;
			default:
				forward(request, response, "carouselses", carouselsService.getCarouselses(),
						"../WEB-INF/jsp/carousels/list.jsp");
		}
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String msg,
						 String url) throws ServletException, IOException {
		request.setAttribute("msg",msg);
		request.getRequestDispatcher(url).forward(request,response);
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String key,
						 Object value, String url) throws ServletException, IOException {
		request.setAttribute(key,value);
		request.getRequestDispatcher(url).forward(request,response);
	}

	private Carousels getCarouselsByRequest(HttpServletRequest request)
			throws IOException, ServletException {
		Carousels carousels = new Carousels();
		String uploadDir = getServletContext().getInitParameter("uploadDir");
		// 网址换成磁盘路径
		String realPath = getServletContext().getRealPath(uploadDir);
		File file = new File(realPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String id = request.getParameter("id");
		if (id != null && id.matches("\\d{1,9}")) {
			carousels.setId(Integer.parseInt(id));
		}
		Part icon = request.getPart("icon");
		if (icon != null && icon.getSize() > 0) {
			String submittedFileName = icon.getSubmittedFileName();
			String fileName = converFileName(submittedFileName);
			carousels.setIcon(fileName);
			icon.write(realPath + "\\" + fileName);
		}
		String serviceId = request.getParameter("serviceId");
		if (serviceId != null && serviceId.matches("\\d{1,9}")) {
			carousels.setService(new Service(Integer.parseInt(serviceId)));
		}
		return carousels;
	}

	private String converFileName(String submittedFileName) {
		SimpleDateFormat simpleFormatter = new SimpleDateFormat("-yyyyMMddhhmmss");
		String temp = simpleFormatter.format(new Date());
		int pos = submittedFileName.lastIndexOf(".");
		return submittedFileName.substring(0, pos) + temp + submittedFileName.substring(pos);
	}

}
