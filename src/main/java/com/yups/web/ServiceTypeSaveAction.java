package com.yups.web;

import com.yups.dao.ServiceTypeDao;
import com.yups.model.ServiceType;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = "/servlet/ServiceTypeSaveAction")
@MultipartConfig
public class ServiceTypeSaveAction extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");
        if(act==null){
            act="list";
        }
        ServiceType serviceType=getServiceTypeFromRequest(request);
        switch (act){
            case "addSave":
                String msg=checkServiceType(serviceType);
                if(!"".equals(msg)){
                        request.setAttribute("act","addSave");
                        request.setAttribute("serviceType",serviceType);
                        forward(request, response, msg,
                                "../WEB-INF/jsp/serviceType/input.jsp");
                        return;
                }
                if(serviceTypeDao.add(serviceType)){
                        forward(request, response,"保存成功",
                                "ServiceTypeAction?act=list");
                    }else{
                        request.setAttribute("act","addSave");
                        request.setAttribute("serviceType",serviceType);
                        forward(request, response, "保存失败",
                                "../WEB-INF/jsp/serviceType/input.jsp");
                    }
                break;
            case "updateSave":
                msg=checkServiceType(serviceType);
                if(!"".equals(msg)){
                    request.setAttribute("act","updateSave");
                    request.setAttribute("serviceType",serviceType);
                    forward(request, response, msg,
                            "../WEB-INF/jsp/serviceType/input.jsp");
                    return;
                }
                if(serviceTypeDao.update(serviceType)){
                    forward(request, response,"保存成功",
                            "ServiceTypeAction?act=list");
                }else{
                    request.setAttribute("act","updateSave");
                    request.setAttribute("serviceType",serviceType);
                    forward(request, response, "保存失败",
                            "../WEB-INF/jsp/serviceType/input.jsp");
                }
                break;
                default://查 无参 (Get,post)
                    forward(request,response,"serviceTypes",serviceTypeDao.getAll(),
                            "../WEB-INF/jsp/serviceType/list.jsp");
        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response,
            String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private void forward(HttpServletRequest request, HttpServletResponse response,
                         String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private String checkServiceType(ServiceType serviceType) {
        String msg="";
        if(serviceType.getTitle()==null ||  !serviceType.getTitle()
                .matches("\\S{1,5}")){
            return  "您输入的标题应该在1-5个字符。";
        }
        return msg;
    }

    private ServiceType getServiceTypeFromRequest(HttpServletRequest request) throws IOException, ServletException {
        String uploadDir = getServletContext().getInitParameter("uploadDir");
        // 网址换成磁盘路径
        String realPath = getServletContext().getRealPath(uploadDir);
        File file = new File(realPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        ServiceType ServiceType=new ServiceType();
        String id = request.getParameter("id");
        if(id!=null && id.matches("\\d{1,9}")){
            ServiceType.setId(Integer.parseInt(id));
        }
        String title = request.getParameter("title");
        ServiceType.setTitle(title);
        Part icon = request.getPart("icon");
        if (icon != null && icon.getSize() > 0) {
            String submittedFileName = icon.getSubmittedFileName();
            String fileName = converFileName(submittedFileName);
            ServiceType.setIcon(fileName);
            icon.write(realPath + "\\" + fileName);
        }
        return ServiceType;
    }

    private String converFileName(String submittedFileName) {
        SimpleDateFormat simpleFormatter = new SimpleDateFormat("-yyyyMMddhhmmss");
        String temp = simpleFormatter.format(new Date());
        int pos = submittedFileName.lastIndexOf(".");
        return submittedFileName.substring(0, pos) + temp + submittedFileName.substring(pos);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
}
