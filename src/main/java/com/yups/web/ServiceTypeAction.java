package com.yups.web;

import com.yups.dao.ServiceTypeDao;
import com.yups.model.ServiceType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/servlet/ServiceTypeAction")
public class ServiceTypeAction extends HttpServlet {
    private ServiceTypeDao serviceTypeDao=new ServiceTypeDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String act = request.getParameter("act");
        if(act==null){
            act="list";
        }
        ServiceType serviceType=getServiceTypeFromRequest(request);
        switch (act){
            case "add":
                forward(request, response, "act","addSave",
                        "../WEB-INF/jsp/serviceType/input.jsp");
                break;
            case "delete":
                forward(request, response,
                        serviceTypeDao.delete(serviceType.getId())
                                ?"删除成功":"删除失败","ServiceTypeAction?act=list");
                break;
            case "update":
                ServiceType serviceType1 = serviceTypeDao.get(serviceType.getId());
                request.setAttribute("serviceType",serviceType1);
                forward(request, response, "act","updateSave",
                        "../WEB-INF/jsp/serviceType/input.jsp");
                break;
                default:
                    forward(request,response,"serviceTypes",serviceTypeDao.getAll(),
                            "../WEB-INF/jsp/serviceType/list.jsp");
        }
    }

    private void forward(HttpServletRequest request, HttpServletResponse response,
            String key,Object value, String url) throws ServletException, IOException {
        request.setAttribute(key, value);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private void forward(HttpServletRequest request, HttpServletResponse response,
                         String msg, String url) throws ServletException, IOException {
        request.setAttribute("msg", msg);
        request.getRequestDispatcher(url).forward(request, response);
    }

    private String checkServiceType(ServiceType serviceType) {
        String msg="";
        if(serviceType.getTitle()==null ||  !serviceType.getTitle()
                .matches("\\S{1,5}")){
            return  "您输入的标题应该在1-5个字符。";
        }
        return msg;
    }

    private ServiceType getServiceTypeFromRequest(HttpServletRequest request) {
        ServiceType ServiceType=new ServiceType();
        String id = request.getParameter("id");
        if(id!=null && id.matches("\\d{1,9}")){
            ServiceType.setId(Integer.parseInt(id));
        }
        String title = request.getParameter("title");
        ServiceType.setTitle(title);
        String icon = request.getParameter("icon");
        ServiceType.setIcon(icon);
        return ServiceType;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }
}
