package com.yups.web;
import com.yups.dao.CoachDao;
import com.yups.model.Coach;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 教练servlect
 *
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/servlet/CoachAction")
public class CoachAction extends HttpServlet {
	private CoachDao coachDao = new CoachDao();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String act = request.getParameter("act");
		if (act == null) {
			act = "list";
		}
		Coach coach = getCoachByRequest(request);
		switch (act) {
			case "add":
				forward(request, response, "act", "addSave",
						"../WEB-INF/jsp/coach/input.jsp");
				break;
			case "addSave":
				String msg = "";
				if (!"".equals(msg)) {
					request.setAttribute("act", "addSave");
					request.setAttribute("coach", coach);
					forward(request, response, msg, "../WEB-INF/jsp/coach/input.jsp");
					return;
				}

				if (coachDao.add(coach)) {
					forward(request, response, "保存成功", "CoachAction?act=list");
				} else {
					request.setAttribute("act", "addSave");
					request.setAttribute("coach", coach);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/coach/input.jsp");
				}
				break;
			case "delete":
				forward(request, response, coachDao.delete(coach.getId()) ?
						"删除成功" : "删除失败", "CoachAction?act=list");
				break;
			case "update":
				Coach coach1 = coachDao.get(coach.getId());
				request.setAttribute("coach", coach1);
				forward(request, response, "act",
						"updateSave", "../WEB-INF/jsp/coach/input.jsp");
				break;
			case "updateSave":
				if (coachDao.update(coach)) {
					forward(request, response, "保存成功", "CoachAction?act=list");
				} else {
					request.setAttribute("act", "updateSave");
					request.setAttribute("coach", coach);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/coach/input.jsp");
				}
				break;
			default:
				forward(request, response, "coaches", coachDao.getAll(),
						"../WEB-INF/jsp/coach/list.jsp");
		}
	}

	private Coach getCoachByRequest(HttpServletRequest request) {
		Coach coach = new Coach();
		String id = request.getParameter("id");
		if (id != null && id.matches("\\d{1,9}")) {
			coach.setId(Integer.parseInt(id));
		}
		coach.setCompany(request.getParameter("company"));
		coach.setPhoto(request.getParameter("photo"));
		coach.setAvatar(request.getParameter("avatar"));
		coach.setNickname(request.getParameter("nickname"));
		String price = request.getParameter("price");
		if (price != null && price.matches("\\d+(\\.\\d{1,2})?")) {
			coach.setPrice(Double.parseDouble(price));
		}
		coach.setMessage(request.getParameter("message"));
		String lat = request.getParameter("lat");
		if (lat != null && lat.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
			coach.setLat(Double.parseDouble(lat));
		}
		String lon = request.getParameter("lon");
		if (lon != null && lon.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
			coach.setLon(Double.parseDouble(lon));
		}
		return coach;
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String key,
						 Object value, String url) throws ServletException, IOException {
		request.setAttribute(key, value);
		request.getRequestDispatcher(url).forward(request, response);
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String msg,
						 String url) throws ServletException, IOException {
		request.setAttribute("msg", msg);
		request.getRequestDispatcher(url).forward(request, response);
	}
}
