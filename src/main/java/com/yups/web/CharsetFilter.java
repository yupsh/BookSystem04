package com.yups.web;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
/**
 * 字符编码过滤器
 *
 * @author 于鹏生
 */
@WebFilter(urlPatterns = "/servlet/*")
public class CharsetFilter implements Filter {
    private String charset;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init");
        charset=filterConfig.getServletContext().getInitParameter("charset");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        // 改变了请求响应的属性
        request.setCharacterEncoding(charset);
        response.setContentType("text/html;charset="+charset);
        // 把请求响应继续下去，不写这句，请求响应过程会停止。
        filterChain.doFilter(request,response);
    }
}
