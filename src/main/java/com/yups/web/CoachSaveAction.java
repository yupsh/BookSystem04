package com.yups.web;

import com.yups.dao.CoachDao;
import com.yups.model.Coach;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 教练servlect
 *
 * @author 于鹏生
 */
@WebServlet(urlPatterns = "/servlet/CoachSaveAction")
@MultipartConfig
public class CoachSaveAction extends HttpServlet {
	private CoachDao coachDao = new CoachDao();

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String act = request.getParameter("act");
		if (act == null) {
			act = "list";
		}
		Coach coach = getCoachByRequest(request);
		switch (act) {
			case "addSave":
				String msg = "";
				if (!"".equals(msg)) {
					request.setAttribute("act", "addSave");
					request.setAttribute("coach", coach);
					forward(request, response, msg, "../WEB-INF/jsp/coach/input.jsp");
					return;
				}

				if (coachDao.add(coach)) {
					forward(request, response, "保存成功", "CoachAction?act=list");
				} else {
					request.setAttribute("act", "addSave");
					request.setAttribute("coach", coach);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/coach/input.jsp");
				}
				break;
			case "updateSave":
				if (coachDao.update(coach)) {
					forward(request, response, "保存成功", "CoachAction?act=list");
				} else {
					request.setAttribute("act", "updateSave");
					request.setAttribute("coach", coach);
					forward(request, response, "保存失败",
							"../WEB-INF/jsp/coach/input.jsp");
				}
				break;
			default:
				forward(request, response, "coaches", coachDao.getAll(),
						"../WEB-INF/jsp/coach/list.jsp");
		}
	}

	// private Coach getCoachByRequest(HttpServletRequest request) {
	// 	Coach coach = new Coach();
	// 	String id = request.getParameter("id");
	// 	if (id != null && id.matches("\\d{1,9}")) {
	// 		coach.setId(Integer.parseInt(id));
	// 	}
	// 	coach.setCompany(request.getParameter("company"));
	// 	coach.setPhoto(request.getParameter("photo"));
	// 	coach.setAvatar(request.getParameter("avatar"));
	// 	coach.setNickname(request.getParameter("nickname"));
	// 	String price = request.getParameter("price");
	// 	if (price != null && price.matches("\\d+(\\.\\d{1,2})?")) {
	// 		coach.setPrice(Double.parseDouble(price));
	// 	}
	// 	coach.setMessage(request.getParameter("message"));
	// 	String lat = request.getParameter("lat");
	// 	if (lat != null && lat.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
	// 		coach.setLat(Double.parseDouble(lat));
	// 	}
	// 	String lon = request.getParameter("lon");
	// 	if (lon != null && lon.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
	// 		coach.setLon(Double.parseDouble(lon));
	// 	}
	// 	return coach;
	// }

	private void forward(HttpServletRequest request, HttpServletResponse response, String key,
						 Object value, String url) throws ServletException, IOException {
		request.setAttribute(key, value);
		request.getRequestDispatcher(url).forward(request, response);
	}

	private void forward(HttpServletRequest request, HttpServletResponse response, String msg,
						 String url) throws ServletException, IOException {
		request.setAttribute("msg", msg);
		request.getRequestDispatcher(url).forward(request, response);
	}

	private Coach getCoachByRequest(HttpServletRequest request)
			throws IOException, ServletException {
		Coach coach = new Coach();
		String uploadDir = getServletContext().getInitParameter("uploadDir");
		// 网址换成磁盘路径
		String realPath = getServletContext().getRealPath(uploadDir);
		File file = new File(realPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		String id = request.getParameter("id");
		if (id != null && id.matches("\\d{1,9}")) {
			coach.setId(Integer.parseInt(id));
		}
		coach.setCompany(request.getParameter("company"));
		// coach.setPhoto(request.getParameter("photo"));
		Part photo = request.getPart("photo");
		if (photo != null && photo.getSize() > 0) {
			String submittedFileName = photo.getSubmittedFileName();
			String fileName = converFileName(submittedFileName);
			coach.setPhoto(fileName);
			photo.write(realPath + "\\" + fileName);
		}
		// coach.setAvatar(request.getParameter("avatar"));
		Part avatar = request.getPart("avatar");
		if (avatar != null && avatar.getSize() > 0) {
			String submittedFileName = avatar.getSubmittedFileName();
			String fileName = converFileName(submittedFileName);
			coach.setAvatar(fileName);
			avatar.write(realPath + "\\" + fileName);
		}
		coach.setNickname(request.getParameter("nickname"));
		String price = request.getParameter("price");
		if (price != null && price.matches("\\d+(\\.\\d{1,2})?")) {
			coach.setPrice(Double.parseDouble(price));
		}
		coach.setMessage(request.getParameter("message"));
		String lat = request.getParameter("lat");
		if (lat != null && lat.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
			coach.setLat(Double.parseDouble(lat));
		}
		String lon = request.getParameter("lon");
		if (lon != null && lon.matches("\\d{1,3}[\\.]?\\d{0,5}")) {
			coach.setLon(Double.parseDouble(lon));
		}
		return coach;
	}

	private String converFileName(String submittedFileName) {
		SimpleDateFormat simpleFormatter = new SimpleDateFormat("-yyyyMMddhhmmss");
		String temp = simpleFormatter.format(new Date());
		int pos = submittedFileName.lastIndexOf(".");
		return submittedFileName.substring(0, pos) + temp + submittedFileName.substring(pos);
	}
}
