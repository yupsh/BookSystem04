package com.yups.web;
import com.yups.dao.BaseDao;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 监听器提前建立数据库连接
 * @author 于鹏生
 */
@WebListener
public class ApplicationListenr implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("应用程序启动");
        new BaseDao();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("应用程序销毁");
    }
}
