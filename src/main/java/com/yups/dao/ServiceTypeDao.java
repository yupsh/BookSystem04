package com.yups.dao;

import com.yups.model.ServiceType;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 菜单栏数据操作类
 *
 * @author 于鹏生
 */
public class ServiceTypeDao extends BaseDao {
	public boolean add(ServiceType serviceType) {
		try {
			String sql = "insert into servicetype ( icon, title)\n"
					+ " values ( ?, ?)";
			return queryRunner.execute(sql,
					serviceType.getIcon(),
					serviceType.getTitle()
			) == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(int id) {
		try {
			String sql = "DELETE FROM servicetype WHERE id = ?";
			return queryRunner.execute(sql, id) == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean update(ServiceType serviceType) {
		try {
			String sql = "UPDATE servicetype SET"
					+ " icon = ?, title = ?\n"
					+ " WHERE id = ?";
			return queryRunner.execute(sql,
					serviceType.getIcon(),
					serviceType.getTitle(),
					serviceType.getId()
			) == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public ServiceType get(int id) {
		try {
			String sql = "select id, icon, title from servicetype where id = ?";
			return queryRunner.query(sql, new BeanHandler<>(ServiceType.class), id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<ServiceType> getAll() {
		try {
			String sql = "select id, icon, title from servicetype order by id";
			return queryRunner.query(sql, new BeanListHandler<>(ServiceType.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
