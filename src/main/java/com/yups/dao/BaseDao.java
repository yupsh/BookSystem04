package com.yups.dao;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.dbutils.QueryRunner;
/**
 * 数据库操作基类
 *
 * @author 于鹏生
 */
public class BaseDao {
	private static ComboPooledDataSource ds;
	protected QueryRunner queryRunner=new QueryRunner(ds);
	
	static {
		try {
			ds=new ComboPooledDataSource();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
